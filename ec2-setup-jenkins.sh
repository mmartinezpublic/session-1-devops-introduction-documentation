#AMAZON IMA Jenkins: 
#AMAZON IMA Sonar: 
#Installation - User data - BEGIN
export MAVEN_VERSION=3.5.2
wget http://www-us.apache.org/dist/maven/maven-3/${MAVEN_VERSION}/binaries/apache-maven-${MAVEN_VERSION}-bin.tar.gz
sudo tar -zxvf apache-maven-${MAVEN_VERSION}-bin.tar.gz -C /opt/
sudo ln -fsT /opt/apache-maven-${MAVEN_VERSION}/bin/mvn /usr/bin/mvn
sudo echo "DOCKER_OPTS=\"-H tcp://0.0.0.0:4243\"" | sudo tee --append /etc/default/docker
sudo service docker restart

##Versions:
mvn --version
# Apache Maven 3.5.0 
java -version
# java version "1.8.0_144"
cat /etc/*-release
#"Ubuntu 14.04.5 LTS"
docker --version
#Docker version 17.05.0-ce, build 89658be
git --version
#git version 2.14.1
##Installation - User data - END

##Data for connectivity
# Jenkins URL
# http://xxxx.xxxxx.compute.amazonaws.com/jenkins/login?from=%2Fjenkins%2F
# user: user
# password: xxxx

# SSH User:
# Host Name: xxxx.xxxxx.compute.amazonaws.com
# Port: 22
# User name: ubuntu
# Private key: demo-devops.pem / demo-devops.ppk

## Where is:
whereis mvn
# mvn: /opt/apache-maven-3.5.0/bin/mvn /opt/apache-maven-3.5.0/bin/mvn.cmd

whereis java
# java: /usr/share/java

whereis docker
# docker: /usr/bin/docker /etc/docker /usr/lib/docker /usr/bin/X11/docker /usr/share/man/man1/docker.1.gz
 
